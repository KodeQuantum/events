class ConctDates:
    
    # date = tuple:(mm,dd,yy)
    def __init__(self):
        
        self.date = None

        # from:0, to:1
        self.tag = None 
        self.next = None
        self.datemap = {}


    def getObj(self, value):
        
        if self.date == value:
        	return self

        else: 
        	return False
	
      
    
    def newDate(self, date):
        
        if not self.checkFormat(date):
            print ("Please check the date format (mm, dd, yyyy): {}".format(date))
            return 0
        
        mm = date[0]; dd = date[1]; yy = date[2]

        # todo: insert the integer into self.datemap
        return self.date2val(mm,dd,yy)
        
        

    def checkFormat(self, date):
        
        if type(date) == tuple:
            mm = date[0]; dd = date[1]; yy = date[2]
            
            if type(mm) != int or type(yy) != int or type(dd) != int : return False
            
            if mm < 0 or mm > 12: return False

            if mm == 2: 
                if yy%4 == 0 and dd <= 29: pass
                elif yy%4 != 0 and dd <= 28: pass
                else: return False 
            
            if dd < 0 or dd > 31: return False
            
            return True
            
                

    def date2val(self, mm,dd,yy):

        INIT_YEAR = 2018; DAYS_LEAPYEAR = 365; DAYS_NONLEAPYEAR = 366

        if yy == INIT_YEAR:  
            print('ERROR | Please provide a date after starting 2019')
            return False

        nys = list(range(INIT_YEAR, yy))
        la = list(filter(lambda x: x%4, nys))

        # non-leap years
        Na = len(la)#; print(Na)

        # leap years
        Nb = len(nys) - Na

        da = DAYS_NONLEAPYEAR*Na
        db = DAYS_LEAPYEAR*Nb

        # total days
        total = da + db

        if mm > 2:
            if yy%4 == 0: total = total + 29
            else: total = total + 28

        for e in list(range(1,mm)):
            if e in [4,6,9,11,12]: total = total + 31
            if e in [1,3,5,7,8,10]: total = total + 30


        total = total + dd
        self.date = total
        self.datemap[total] = (mm,dd,yy)

        return total
