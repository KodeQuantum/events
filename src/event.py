import node as nd
import conctdates as cd

class Event:
    
    def __init__(self):
        
        self.from_tree = None 
        self.to_tree = None 
     

    def insertOne(self, cf, ct):
        
        cf.next = ct; ct.next = cf
        cf.tag = 0; ct.tag = 1
        self.from_tree = nd.insertNode(self.from_tree, cf)
        self.to_tree = nd.insertNode(self.to_tree, ct)
        print('Insert successful - from:{0}-to:{1}'.format(cf.datemap[cf.date], ct.datemap[ct.date] ))
        return True 



    # dates = (from, to), from=(mm,dd,yyyy), to=(mm,dd,yyyy)
    def addEvent(self, dates):
        dfrom = dates[0]
        dto = dates[1]
        
        cf = cd.ConctDates()
        cfv = cf.newDate(dfrom)
        
        ct = cd.ConctDates()
        ctv = ct.newDate(dto) 


        # if date format is incorrect 
        # 0 comes from executing the newDate
        if cfv == 0 or ctv == 0:
            return False
        
        if cfv > ctv :
            print("ERROR | To:Date{1} is before the From:Date{0}".format(dfrom, dto))
            return False
            
        if cfv == ctv : 
            print("ERROR | To and From dates are identical") 
            return False

       
              
        if not self.from_tree and not self.to_tree : 
            self.insertOne(cf,ct)
            return True 

        tf = nd.getObj(self.from_tree, cfv) 
        tv = nd.getObj(self.to_tree, ctv) 

        if tf or tv: 
            print("\nERROR | From:{0}-{1}, To:{2}-{3}".format(dfrom, tf, dto, tv))
            print("Either/Both the dates exist, \nTrue:\'already exists\', False:\'doesn't exist\'")
            return False

        
        # get everything more/less than the given value
        rt,lt = nd.objsGL(self.from_tree, cfv, rt=set(), lt=set()) 
       
        lrt = len(rt); llt = len(lt); 
        if lrt > 0 and llt > 0: 
            rmin = min(rt); rmax = max(rt)
            lmin = min(lt); lmax = max(lt)

            olmax = nd.getObj(self.from_tree, lmax, obj=True)
            if type(olmax) != cd.ConctDates: return False
            if cfv > olmax.next.date and ctv < rmin:
                    return self.insertOne(cf, ct)
            else: return False


        elif llt > 0 and lrt == 0: 

            lmin = min(lt); lmax = max(lt)

            olmax = nd.getObj(self.from_tree, lmax, obj=True)
            if type(olmax) != cd.ConctDates: return False
            if cfv > olmax.next.date :
                    self.insertOne(cf, ct)
                    return True 
            else: return False


        elif lrt > 0 and llt == 0: 

            rmin = min(rt); rmax = max(rt)

            ormin = nd.getObj(self.from_tree, rmin, obj=True)
            if type(ormin) != cd.ConctDates: return False
            if ctv < ormin.next.date :
                    return self.insertOne(cf, ct)
            else: return False



    def getGaps(self): 

        import datetime
        cl = nd.readData(self.from_tree, ktype='clist', ldata=[])

        if cl != None and len(cl) == 0: 
            print("ERROR | No objects found") 
            return 

        dt = datetime.datetime
        dn = dt.now()
        cd = (dn.month, dn.day, dn.year)

        # give utc timezone by default
        print('\nThe following slots are available: ')
        vt = cl[0].next; 
        print('starting today:{0} - {1} '.format(cd,vt.datemap[vt.date]))

        for ce in cl[1:]:

            vf = ce.date
            #diff = vf - vt; 
            print(vt.datemap[vt.date],'-', ce.datemap[vf])
            vt = ce.next
        
        last = len(cl)-1

        print('and all dates after: ', cl[last].datemap[cl[last].date])
        
        print('\n\n========================================================')
        print('Note: the dates are all exclusive, i.e. \neither a date before or a date later than the shown dates are allowed, \n\nfor example :(3,15,2019)-(4,14,2019) shown as available, \nthen (3,16,2019)-(4,13,2019) are the actual date slots...\n')
        print('========================================================\n')

