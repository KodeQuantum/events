class Node:

    def __init__(self,label=None):

        self.left = None
        self.right = None
        self.data = None
        self.label = label
        self.dates = []

        
    def putData(self, data):
        self.data = data
        return self

    def getData(self):
        return self.data



# Node is inserted and the base tree is returned 
def insertNode(node, data):

    root = node; cc = 1
    while (node): 

        if node.data:

            if node.data.date < data.date:
                if node.right is None: 
                    node.right = Node().putData(data)
                    return root

                else: node = node.right

            elif node.data.date > data.date:
                if node.left is None: 
                    node.left = Node().putData(data)
                    return root

                else: node = node.left

            else: return root

    root = Node().putData(data)    
    return root



def readData(root, ktype='value', ldata=[]):

    if root:

        if root.left: readData(root.left, ktype=ktype, ldata=ldata)    

        if root.data: 
            if ktype == 'value': print(root.data.date, end=' ')
            if ktype == 'date': print(root.data.datemap[root.data.date], end=" ")

            if ktype == 'clist': ldata.append(root.data)
            if ktype == 'vlist': ldata.append(root.data.datemap[root.data.date])
            if ktype == 'dlist': ldata.append(root.data.date)

        if root.right: readData(root.right, ktype=ktype, ldata=ldata)

        if ktype in ['clist','vlist','dlist']: return ldata



def getObj(root, value, obj=False):

    if root:

        if root.data:
           if root.data.date == value:
               if obj: return root.data
               else: return True

            #else: return False 


        if root.left :
            if root.left.data :
                #print('root.left')
                if root.left.data.date == value:
                    if obj : return root.left.data
                    else: return True
                #else: return False

            return getObj(root.left, value, obj=obj)



        if root.right :
            if root.right.data :
                #print('root.right')
                if root.right.data.date == value:
                    if obj : return root.right.data
                    else: return True
                #else: return False

            return getObj(root.right, value, obj=obj)

        return False



def objsGL(root, value, rt=set(), lt=set()):

    if root.left: objsGL(root.left, value, rt=rt, lt=lt)

    if root and root.data: 
        if value < root.data.date: rt.add(root.data.date)
        if value > root.data.date: lt.add(root.data.date)

    if root.right: objsGL(root.right, value, rt=rt, lt=lt)

    return rt, lt

