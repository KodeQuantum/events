## Event Insertion and Gap Finding

**Algorithm**: Two Binary Search Trees(BST) are used for inserting dates, one for _from:date_, and another for _to:date_. Tree data structure ensures that complexity reduces to $\mathcal{O}(n) = \mathrm{log}_2 n$. Three classes are used:

* `Event`: Objects of this class use the instances created from Node, and ConctDates
* `ConctDates`: Class that handles the date conversion from (mm,dd,yyyy) to an integer
* `Node`: BSTs allowing objects from ConctDates, inserting them in sequence

The functionality is such that _to_ and _from_ dates form a `Circular Linked List` with an additional tag to differentiate if it's a to or a from date. 

NOTE: Two BSTs are used because future `business cases` are anticipated, like counting the dates from beginning/end especially when there are so many schedules made, i.e. analytics to help user know what's available. Could potentially help with the front-end developer

## Basic APIs
* `Event.addEvent(fromDate, toDate)`, {from,to}Dates are tuples, (mm,dd,yyyy)
* `Event.getGaps()` : Insert some dates, and then get the gaps

### Feature Enhancements
* date conversion from an integer and back to the actual date will help reduce the storage in the base object
* deletion in the Node class helps reorganize the dates

**Running Instructions**

Run `python3 tests/unittests_main.py`, `python3 tests/unittests_conctdates.py`

Outputs: *conctdates.stdout, main.stdout*

* Tested successfully with `Python version > 3.6`
* No external dependencies
