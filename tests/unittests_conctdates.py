# Unit Tests for the class ConctDates
# -----------------------------------
# This class converts a given date starting 2018 into a given integer
# Cross checks are not exhaustive: 
# this is a simple file for testing the functionality of the class for dates within 2019

# functions:(checkformat, date2val) requires more cross-checks, esp for feburary and 
# and to extend to years beyond 2018

# date format is a tuple (mm,dd,yyy)
# eg: (3,5,2019)

# Getting module from local path
import sys
mpath = '../src/'
sys.path.append(mpath)


# Importing user defined class as module
import conctdates as cd

cc = 0
obj = cd.ConctDates()

# will work
date = (3,5,2019)
print(cc,  ': date:{} converted to an integer'.format(date))
print(obj.newDate(date))

# will fail
print('\n'); cc += 1
date = (53,50,2019)
print(cc,  ': date:{} converted to an integer'.format(date))
print(obj.newDate(date))

# will work, returns True/False
print('\n');cc += 1
date = (53,50,2019)
print(cc,  ': date:{} converted to an integer'.format(date))
print(obj.checkFormat(date))

# will work, returns True/False
print('\n'); cc += 1
date = (3,5,2019)
print(cc,  ': date:{} converted to an integer'.format(date))
print(obj.checkFormat(date))

# non leap year can't have feb more than 29 days
print('\n'); cc += 1
date = (2,29,2019)
print(cc,  ': date:{} converted to an integer'.format(date))
print(obj.checkFormat(date))

# leap year feb allowed 29 days
print('\n'); cc += 1
date = (2,29,2024)
print(cc,  ': date:{} converted to an integer'.format(date))
print(obj.checkFormat(date))

# etc
