# Unit Tests
# ----------
# Note: Tests are not exhaustive
# Actual functionality is here, the classes hold information about objects from other classes


# Getting module from local path
import sys
mpath = '../src/'
sys.path.append(mpath)


import node as nd
import event as et
import conctdates as cd


print("\n\n-----------EVENT INSERTION TESTS-------------")
# whenever an insert is successful, returns True, else an error message is printed returning a False
e = et.Event()

# Sequence of events that are successfully inserted
# non overlapping dates
e.addEvent(((3,1,2019),(3,15,2019)))
e.addEvent(((7,30,2019),(8,30,2019)))


print('\n')
e = et.Event()
# inserts
e.addEvent(((3,1,2019),(3,15,2019)))
# fails
e.addEvent(((3,6,2019),(8,30,2019)))
# fails
e.addEvent(((2,6,2019),(3,30,2019)))

# inserting existing values: throws messages
print('\n')
e.addEvent(((3,1,2019),(3,15,2019)))
e.addEvent(((2,1,2019),(2,20,2019)))
e.addEvent(((7,30,2019),(8,30,2019)))


print('\n')
e = et.Event()

e.addEvent(((3,1,2019),(3,15,2019)))
e.addEvent(((2,1,2019),(2,20,2019)))
e.addEvent(((7,30,2019),(8,30,2019)))
e.addEvent(((9,15,2019),(9,25,2019)))
e.addEvent(((9,26,2019),(10,12,2019)))

print("\n\n-----------GAPS BETWEEN INSERTED EVENTS-------------")
e.getGaps()


print('\n\nDates for years after 2019 and with leap year')
print('Contains dates in arbitrary sequence demonstrating insertion sequence\n')

e = et.Event()

def checks(e, df,dt):
    p = e.addEvent((df,dt))
    print('{2} :\t e.addEvent(({0},{1}))'.format(df,dt,p), end="\n\n")

    

e = et.Event()
df = (3,2,2025); dt = (3,9,2025); checks(e, df ,dt)
df = (4,2,2025); dt = (4,9,2026); checks(e, df ,dt)
df = (8,2,2025); dt = (8,9,2025); checks(e, df ,dt)
df = (3,15,2025); dt = (3,20,2025); checks(e, df ,dt)
df = (1,12,2020); dt = (3,20,2022); checks(e, df ,dt)
df = (1,12,2018); dt = (3,20,2018); checks(e, df ,dt)


print("\n\n-----------GAPS BETWEEN INSERTED EVENTS-------------")
e.getGaps()
