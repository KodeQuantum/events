**June 18, 2019**

* ConctDates.date2val extended for 2019 and beyond
* Refactored class Events.addEvent and bug fix for getGaps
* Local import of user defined classes
* Unit tests moved to another directory named `test`
